from flask import Flask, render_template, request, session
from datetime import datetime
import json

from client import BibliaClient
import settings

app = Flask(__name__)
app.secret_key = settings.SECRET_KEY


def get_votd():
    date = datetime.now().date()
    # convert date to string
    now_int = int(f"{date.year}{date.month}{date.day}")
    # Use mod of the date and VOTD length to get a index which will increment each day
    votd_index = now_int % len(settings.VOTD)
    return settings.VOTD[votd_index]


def create_client():
    client = BibliaClient()
    client.bible_version = session.get("bible_version", settings.DEFAULT_BIBLE_VERSION)
    return client


@app.route('/', methods=["GET", "POST"])
def index():
    passage = get_votd()

    version = request.form.get("version")
    if version:
        session["bible_version"] = version

    if not session.get("bible_version"):
        session["bible_version"] = settings.DEFAULT_BIBLE_VERSION

    client = create_client()
    context = {
        "votd": client.get_passage(passage, verse_number=False),
        "bibles": settings.BIBLE_OPTIONS,
        "bible_version": session.get("bible_version")
    }
    return render_template("index.html", **context)


@app.route('/search', methods=["GET", "POST"])
def search():
    client = create_client()
    # Use the lookup API to normalize the pasage reference
    result = client.lookup(request.form.get("query"))

    if result.get("passages"):
        # This is a reference, use get_passage to return text for each reference
        verses = []
        for passage in result["passages"]:
            result = client.get_passage(passage["passage"])
            verses.append(result)
    else:
        # Not a reference so use the search API instead
        result = client.search(request.form.get("query"), request.form.get("start"))
        verses = []
        for passage in result["results"]:
            verses.append({
                "ref": passage["title"],
                "text": passage["preview"]
            })
    return json.dumps(verses)


if __name__ == '__main__':
    app.run()
