# BibleLookup

Verse of the day and Bible search website based on the Biblia API. It's built with Flask and Vue.JS.

## Install

To use you'll need a Biblia API Token. If you don't have one you can register here https://bibliaapi.com/docs/API_Keys.
 Edit the `settings.py` file and add your API token to the `BIBLIA_TOKEN` line, you'll also need to set the `SECRET_KEY`, this should just be a random string.

You can use either  Docker or just install it onto your machine.

### Machine Install

BibleLookup requires Python 3.6 or greater to run. In the root of the repo run the following commands.

Install the requirements

```pip install -r requirements.txt```

Run the site

```flask run```

The site should now be up on localhost:5000.


### Docker install

You'll need to install both docker, docker-compose before continuing. In the root of the repo run the following to buid the image:

```docker-compose build```

Then run the website using the following command.

```docker-compose up```

The site should now be up on localhost:5000. 


