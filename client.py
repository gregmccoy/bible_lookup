from settings import BIBLIA_TOKEN

import requests


class BibliaClient(object):
    bible_version = ""

    def call_api(self, endpoint, params):
        params["key"] = BIBLIA_TOKEN

        response = requests.get(endpoint, params=params)

        if response.status_code != 200 and response.status_code != 201:
            raise requests.ConnectionError("Expected status code 200, but got {}".format(response.status_code))
        return response.json()

    def get_passage(self, passage, verse_number=True):
        endpoint = f"https://api.biblia.com/v1/bible/content/{self.bible_version}.text.json"
        params = {"passage": f"{passage}"}
        if verse_number:
            # HTML gets removed by the biblia API, so adding -sup- as a marker that wil get swapped out after
            params["eachVerse"] = "-SUP-[VerseNum]-ENDSUP-[VerseText]"

        response = self.call_api(endpoint, params)
        response["ref"] = params["passage"]
        response["text"] = response["text"].replace("-SUP-", "<sup>")
        response["text"] = response["text"].replace("-ENDSUP-", "</sup>")
        return response

    def lookup(self, query):
        params = {"passage": f"{query}"}
        return self.call_api("https://api.biblia.com/v1/bible/parse/", params)

    def search(self, query, start):
        if not start:
            start = 0
        endpoint = f"https://api.biblia.com/v1/bible/search/{self.bible_version}.text.json"
        params = {
            "query": f"{query}",
            "start": f"{start}",
            "limit": 10,
        }
        return self.call_api(endpoint, params)
