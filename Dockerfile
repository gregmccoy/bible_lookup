FROM ubuntu

RUN apt-get update
RUN apt-get install -y python3 python3-pip

EXPOSE 5000

ADD requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt

ADD . /app

WORKDIR /app

CMD flask run -h 0.0.0.0
