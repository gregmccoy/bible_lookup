BIBLIA_ENDPOINT = "https://api.biblia.com/v1/bible/content/"

BIBLE_OPTIONS = (
    "ASV",
    "KJV",
    "DARBY",
    "EMPHBBL",
    "KJV1900",
    "LEB",
    "YLT",
)
DEFAULT_BIBLE_VERSION = "ASV"


VOTD = (
    "2 Corinthians 9:7",
    "John 3:16",
    "Romans 8:31",
    "Hebrews 12:14",
    "Ecclesiastes 9:10",
    "Galatians 6:16",
    "1 Peter 4:14",
    "Isaiah 46:9",
    "1 Chronicles 16:12",
    "John 15:13",
)

BIBLIA_TOKEN = ""
SECRET_KEY = ""

try:
    from production_settings import *
except ImportError:
    pass
